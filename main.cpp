/*
  Explanation: The output below was generated on a single-core machine. When
  thread1 starts, it enters the loop for the first time and calls increment()
  followed by get(). However, before it can print the returned value to
  std::cout, the scheduler puts thread1 to sleep and wakes up thread2, which
  obviously has time enough to run all three loop iterations at once. Back to
  thread1, still in the first loop iteration, it finally prints its local copy
  of the counter's value, which is 1, to std::cout and then runs the remaining
  two loop iterations. On a multi-core machine, none of the threads is put to
  sleep and the output is more likely to be in ascending order.

  URL: https://omegaup.com/docs/cpp/en/cpp/thread/shared_mutex.html
  IDE: Visual Studio Code
  Build Utility: Makefile and Cygwin64 --> https://www.cygwin.com/
  C++: Modern C++ 17 

  Possible Output using Cygwin64:
  $ make
  $ g++ -Wall -std=c++17 ../main.cpp -o ReaderWriter
  $ ./ReaderWriter.exe
  0x800012930 1
  0x800012930 2
  0x800012930 3
  0x800012930 4
  0x800012930 5
  0x800012930 6
  0x800012c30 7
  0x800012c30 8
  0x800012c30 9
  0x800012c30 10
  0x800012c30 11
  0x800012c30 12
  0x800012d80 13
  0x800012d80 14
  0x800012d80 15
  0x800012d80 16
  0x800012d80 17
  0x800012d80 18
*/

#include <iostream>
#include <mutex>  // For std::unique_lock
#include <shared_mutex>
#include <thread>
 
class ThreadSafeCounter {
 public:
  ThreadSafeCounter() = default;
 
  // Multiple threads/readers can read the counter's value at the same time.
  unsigned int get() const {
    std::shared_lock lock(m_mutex);
    return m_value;
  }
 
  // Only one thread/writer can increment/write the counter's value.
  void increment() {
    std::unique_lock lock(m_mutex);
    m_value++;
  }
 
  // Only one thread/writer can reset/write the counter's value.
  void reset() {
    std::unique_lock lock(m_mutex);
    m_value = 0;
  }
 
 private:
  mutable std::shared_mutex m_mutex;
  unsigned int m_value = 0;
};
 
int main() {
  ThreadSafeCounter counter;
 
  auto increment_and_print = [&counter]() {
    for (int i = 0; i < 6; i++) {
      counter.increment();
      std::cout << std::this_thread::get_id() << ' ' << counter.get() << '\n';

      // Note: Writing to std::cout actually needs to be synchronized as well
      // by another std::mutex. This has been omitted to keep the example small.
    }
  };
 
  std::thread thread1(increment_and_print);
  std::thread thread2(increment_and_print);
  std::thread thread3(increment_and_print);

  thread1.join();
  thread2.join();
  thread3.join();
}